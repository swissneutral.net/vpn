#!/bin/bash

###########################################################################
# Définitions des variables globales privées propres à la machine virtuelle
###########################################################################

# Liste des administrateurs de cette machine
# ADMINS_LIST="user_1 user_2"

# ssh
SSH_PORT="22"

# Liste des ports à ouvrir
FIREWALL_UDP_PORT="53 853"
FIREWALL_TCP_PORT="53 853"
