#!/bin/bash

#######################################################################################

# variables a modifier #

# apt-get install quilt unifdef build-essential libssl-dev flex bison gcc-arm-linux-gnueabihf u-boot-tools qt5-default

set -e

# Variable to update
use_debian_source="true"
v_kernel="4.19.98"
v_kernel_2=$v_kernel
debian_version="$v_kernel-1" ## linux_4.9.13-1~bpo8+1.debian.tar.xz
# debian_version="$v_kernel-1+deb9u1" ## linux_4.9.13-1~bpo8+1.debian.tar.xz
# debian_version="$v_kernel-1~bpo9+1" ## linux_4.9.13-1~bpo8+1.debian.tar.xz
# debian_version="$v_kernel-3+deb9u1~bpo8+1" ## linux_4.9.13-1~bpo8+1.debian.tar.xz
v_cryptodev="1.8"

# v_kernel="4.14~rc5"
# v_kernel_2="4.14.0-rc5"
# debian_version="$v_kernel-1~exp1" ## linux_4.9.13-1~bpo8+1.debian.tar.xz

# Constant
# cross_compiler_path="/home/josue/Programmes_Materiel/Developpement/Systeme_mirabox/fichier/Marvell_toolchain_201201/armv7-marvell-linux-gnueabi-softfp_i686/bin"
# cross_compiler="arm-marvell-linux-gnueabi-"
cross_compiler_path="/usr/lib/gcc-cross/arm-linux-gnueabihf/8"
cross_compiler="arm-linux-gnueabihf-"

base_directory="/home/josue/Programmes_Materiel/Developpement/Systeme_mirabox/kernel/$v_kernel"
source_kernel="$base_directory/linux-$v_kernel"
patch_path="/home/josue/Programmes_Materiel/Materiel/Mirabox/mvneta_Patch"

#######################################################################################

if [[ ! -e $source_kernel ]]
then
    echo "Téléchargemetn des sources"

    mkdir -p $base_directory
    cd $base_directory
    
	if [[ $use_debian_source = "true" ]]
	then
		wget http://http.debian.net/debian/pool/main/l/linux/linux_$v_kernel_2.orig.tar.xz
		wget http://http.debian.net/debian/pool/main/l/linux/linux_$debian_version.debian.tar.xz
		tar -xf linux_$v_kernel_2.orig.tar.xz
		tar -xf linux_$debian_version.debian.tar.xz
		mv debian linux-$v_kernel/

		echo "Préparation de la source"

		cd "$source_kernel"
		make -f debian/rules orig
    else
		wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$v_kernel.tar.xz
		tar -xf linux-$v_kernel.tar.xz
		
		cd "$source_kernel"
    fi
fi

cd "$source_kernel"

echo "Configuration du kernel"

read -p 'vouler vous recréer le fichier .config ?  yes/no/old (oldconfig)  ' creat_config

if [ "$creat_config" = "yes" ]
then

	PATH="$cross_compiler_path:$PATH" make ARCH=arm CROSS_COMPILE=$cross_compiler mvebu_v7_defconfig

elif [ "$creat_config" = "old" ]
then

	PATH="$cross_compiler_path:$PATH" make ARCH=arm CROSS_COMPILE=$cross_compiler oldconfig
	
fi

echo "lancement de la configuration du kernel :"

PATH="$cross_compiler_path:$PATH" make ARCH=arm CROSS_COMPILE=$cross_compiler xconfig
# PATH="$cross_compiler_path:$PATH" make ARCH=arm CROSS_COMPILE=$cross_compiler menuconfig

PATH="$cross_compiler_path:$PATH" make -j4 ARCH=arm CROSS_COMPILE=$cross_compiler zImage

PATH="$cross_compiler_path:$PATH" make ARCH=arm CROSS_COMPILE=$cross_compiler armada-370-mirabox.dtb


cp "arch/arm/boot/zImage" "zImage-with-dtb"

cat "arch/arm/boot/dts/armada-370-mirabox.dtb" >> "zImage-with-dtb"

./scripts/mkuboot.sh -A arm -O linux -T kernel -C none -a 0x00008000 -e 0x00008000 -n "Linux-marvell $v_kernel" -d zImage-with-dtb uImage 

echo "création des modules :"

PATH="$cross_compiler_path:$PATH" make -j4 ARCH=arm CROSS_COMPILE=$cross_compiler modules

PATH="$cross_compiler_path:$PATH" make ARCH=arm CROSS_COMPILE=$cross_compiler INSTALL_MOD_PATH=.. modules_install

exit 0
