
#####################################################################
# Définitions des variables globales privées communes à toutes les vm
#####################################################################

####################
# autossh
####################

# user@adresse_ipv4_serveur_distant:Port|Port_ecoute_distant|Nom_serveur_distant
AUTOSSH_TARGET_IP4="
user@1.2.3.4:1111|1112|nom-machine
"

# user@[adresse_ipv6_serveur_distant]:Port|Port_ecoute_distant|Nom_serveur_distant
AUTOSSH_TARGET_IP6="
user@[1234:5678:abcd::1]:2222|2223|no-machine-v6
"

####################
# Names
####################

ASSOS_EMAIL="add@domain.tld"
ADMIN_EMAIL="add@domain.tld"
ADMIN_EMAIL_DNS=${ADMIN_EMAIL/@/.}

####################
# Names
####################

# VM 304
# List of day of week separeted by ','
# Note that Monday = 0 -> Sunday = 6
BACKUP_VM_304_DOW="2"
BACKUP_VM_304_START_HOURS="00:45"
BACKUP_VM_304_STOP_HOURS="03:00"

####################
# Network
####################

# DNS servers
DNS_SERVER_1_IP4="${SNN_PUBLIC_IPV4_PREFIX}1.1"
DNS_SERVER_2_IP4="${SNN_PUBLIC_IPV4_PREFIX}1.1"
DNS_SERVER_1_IP6="${SNN_PUBLIC_IPV6_PREFIX}1::1"
DNS_SERVER_2_IP6="${SNN_PUBLIC_IPV6_PREFIX}1::1"

####################
# OpenVPN
####################

VPN_1_SERVER_PUB_IP4="${SNN_PUBLIC_IPV4_PREFIX}1.1"
VPN_1_SERVER_PUB_IP6="${SNN_PUBLIC_IPV6_PREFIX}1::1"

# Coin access got fetch user access
VPN_1_COIN_USERNAME_ACCESS="abcd"
VPN_1_COIN_PASSWORD_ACCESS="mypassword"

####################
# ssh
####################

# Liste des administrateurs de cette machine
ADMINS_LIST="florian josue marc mathiasba"

# ssh-vpn
SSH_VPN_SERVER1_ADDRESS_IP4="10.10.10.3"
SSH_VPN_SUBNET1_IP4="10.10.20.0"
SSH_VPN_NETMASQ1_IP4="24"
SSH_VPN_SERVER1_ADDRESS_IP6="${SNN_PUBLIC_IPV6_PREFIX}0::1"
SSH_VPN_SUBNET1_IP6="${SNN_PUBLIC_IPV6_PREFIX}0::"
SSH_VPN_NETMASQ1_IP6="80"

SSH_VPN_SERVER2_ADDRESS_IP4="10.10.10.29"
SSH_VPN_SUBNET2_IP4="10.10.21.0"
SSH_VPN_NETMASQ2_IP4="24"
SSH_VPN_SERVER2_ADDRESS_IP6="${SNN_PUBLIC_IPV6_PREFIX}0::1"
SSH_VPN_SUBNET2_IP6="${SNN_PUBLIC_IPV6_PREFIX}0::"
SSH_VPN_NETMASQ2_IP6="80"

# Custom rule, should be overrided by the vm config
SSH_CUSTOM_RULES=""

####################
# firewall
####################

# Custom rule, should be overrided by the vm config
FIREWALL_IP4_CUSTOM_RULE=""
FIREWALL_IP6_CUSTOM_RULE=""
