#!/bin/bash

# Use this script to upload the vpn/ folder from local computer to the vm and run main.sh

# Need RSA keys installed on vm for remote ssh loggin without password
# ssh-keygen, copy local user id_rsa.pub to /root/.ssh/authorized_keys of the VM

# Need rsync installed on local and distant host
# apt-get install -y rsync




####################
# TO CHANGE BEFORE RUN
####################
remote_host="snndev.aymonsam.ch"	# Address of the remote server
port="21XX"             # SSH port of vm
cmd="INSTALL"           # INSTALL, UPDATE, BACKUP, RESTORE
sys_type="vm"           # Could be 'vm' or 'node'
sys_NUM="8"             # VM_NUM (0 exemple), see 'config' folder
####################

# Local git folder
local_path="./"

# Where to past remote git folder
remote_path="/root/scripts/"

# User login to vm
user="root"

# Install Rsync
ssh -p $port $user@$remote_host "apt-get update && apt-get install -y rsync git"

# Sync local git folder with remote vm
rsync --filter=':- .gitignore' -avz -e "ssh -p $port" --delete $local_path/ $user@$remote_host:$remote_path

# Run main.sh on remote vm
ssh -p $port $user@$remote_host2 "cd $remote_path && chown -R root:root . && ./main.sh $cmd $sys_type $sys_NUM"

# Save backup folder
#if [ "$cmd" -eq "BACKUP" ]; then
#	# Save backup folder
#	:
#fi

# Delete remote folder
ssh -p $port $user@$remote_host "rm -rf $remote_path"

# Reboot
ssh -p $port $user@$remote_host "reboot"
