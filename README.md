Installation de la machine virtuelle N°NUM :
------------------------
En root sur la machine virtuelle:

```
apt-get install git-core -y
git clone https://framagit.org/swissneutral.net/vpn.git
cd vpn/

./main.sh INSTALL NUM	# Replace NUM by the vm_NUM folder number
```

OU

Sur son ordinateur local:

```
git clone https://framagit.org/swissneutral.net/vpn.git
cd vpn/

#Editer le fichier remote_install.sh

./remote_install.sh
```

# Explications des repos :

La structure de ce dépot est définie comme suit :

Répertoire "backup" :
------------------------

Répertoire où seront créées les sauvegardes des différents services d'une machines virtuelle.


Répertoire "config" :
------------------------

- Contient les dossiers de configuration des machines virtuelles.
- Contient les variables globales.


Répertoire "doc" :
------------------------

Contient de la documentation.


Répertoire "services" :
------------------------

- Contient les différents services qui peuvent être installé sur les machines virtuelles.
- Contient les fichiers nécéssaires à l'installation des services.
- Contient les scripts de déployements des différents services.
- Contient un README qui décrira brièvement le service ainsi que les cas de tests de déployements


Fichier "main.sh" :
------------------------

Script principal d'installation des services sur une vm.

Utilisation:
./main.sh CMD NUM


CMD: commande a executer pour les services de cette machine :

- INSTALL	- Installe les services listés dans le fichier config/vm_NUM/config.sh
- UPDATE	- Met à jour les services sur une machine en fonctionnement
- BACKUP	- Sauvegarde la configuration des services dans le répertoire backup
- RESTORE	- Restore une configuration du répertoire backup sur une machine en service


NUM: numéro du dossier de configuration de la machine virtuelle - config/vm_NUM

Exemple dossier vm_7 :
./main.sh CMD 7


Fichier "remote_install.sh" :
------------------------

A utiliser sur son ordinateur local. Copie le répertoire vpn/ vers la machine virtuelle et lance le scripte main.sh.

A utiliser pour des tests lors de développements de services.

A utiliser pour l'installation a distance depuis une machine locale.

Ouvrir le fichier et changez les valeurs internes avant d'exécuter. (Provisoire)

# Structure des VM :

Chaques VM aura une utilité définie. Voici la liste des vm avec leur utilité :

Se référer à "config/README.md".

# Structure des services :

Chaques services est définit dans le répertoire "services".
Il est nécessaire de créer un nouveau répertoire pour chaque service comprenant un script d'installation.
Voici la liste des différents services sous la forme "service_name - service_function"

Les des différents services avec leurs status :

| Nom service       | Description                                               | Etat développements                   |
|-------------------|-----------------------------------------------------------|---------------------------------------|
| autossh           | Service permettant un tunnel de secours d'accès à notre infra | Voir CONTRIBUTING.md              |
| bind              | Serveur DNS authoritaire master                           | Voir CONTRIBUTING.md                  |
| bind_slave        | Serveur DNS authoritaire slave                            | Voir CONTRIBUTING.md                  |
| fail2ban          | Protection attaque                                        | Voir CONTRIBUTING.md                  |
| firewall-router   | configuration du firewall-router d'entrée de l'infra      | Voir CONTRIBUTING.md                  |
| firewall          | iptables, ip6tables (firewall interne à chaque VM)        | Voir CONTRIBUTING.md                  |
| frrouting         | Routage dynamique BGP - Annonces BGP                      | Voir CONTRIBUTING.md                  |
| hwraid            | Service permettant de surveiller nos disque physique (équivalemt à smartd) | OK                   |
| ldap              | LDAP Server                                               | Abandonné au profit de yunohost       |
| ldapadmin         | Admin LDAP                                                | Pour le moment pas vraiment nécessaire |
| locales           | Configuration des locales corrects                        | OK                                    |
| maraidb           | serveur base de donnée                                    | abandonné au profit de yunohost       |
| nginx             | proxy http                                                | abandonné au profit de yunohost       |
| openvpn-cert      | gestion des certificats openvpn                           | Voir CONTRIBUTING.md                  |
| openvpn           | OpenVPN Server                                            | Voir CONTRIBUTING.md                  |
| quagga            | Routage dynamique BGP - Annonces BGP                      | abandonné au profit de ffrouting      |
| resolver          | Résolveur DNS                                             | Voir CONTRIBUTING.md                  |
| security_update   | Configuration des mises a jours de sécurité automatique + mail pour autres mises à jours | Voir CONTRIBUTING.md |
| ssh               | OpenSSH server                                            | Voir CONTRIBUTING.md                  |
| time              | Set time zone and ntp server                              | OK	02.01.2019	                |
| tmux              | Terminal virtuel                                          | Voir CONTRIBUTING.md                  |
| yunohost          | Serveur Yunohost                                          | Voir CONTRIBUTING.md                  |

