Descriptif du service
=====================

Service permettant de gérer les certificats openvpn facilement.

Info particulière déployement
-----------------------------


Cas de test après dépoyement
----------------------------

- Il s'agit de tester toutes les fonctionnalités offertes par le script soit :
    - "first-launch" : initialisation des certificats
    - "create-ca" : creation d'une CA
    - "create-client-cert" : création d'un certificat client + signature par la CA openvpn.
    - "sign-extern-client-cert" : signature d'un certificat d'un client par la CA openvpn.


TODO
----

- Tests