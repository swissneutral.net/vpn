#!/bin/bash

##################################################
# Service - openvpn certification
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################

set_permission() {
    # Set permission for cert_manager scripts
    chmod u+x,g=,o= /root/cert_manager.sh
    chown root:root /root/cert_manager.sh
}

setup_easyrsa() {
    previous_pwd=$(pwd)
    pushd $(mktemp -d)
    wget https://github.com/OpenVPN/easy-rsa/releases/download/v$EASYRSA_VERSION/EasyRSA-$EASYRSA_VERSION.tgz
    tar -xzf EasyRSA-$EASYRSA_VERSION.tgz
    rm EasyRSA-$EASYRSA_VERSION.tgz
    rm -rf /opt/EasyRSA-*
    mv EasyRSA-$EASYRSA_VERSION /opt
    popd
}

####################
# Install
####################

PREINSTALL()
{
	apt-get install openssl openvpn -y
	setup_easyrsa
}

POSTINSTALL() {
    set_permission
    echo "You need to build manually the certificate"
}


####################
# Update
####################

PREUPDATE()
{
	setup_easyrsa
}

POSTUPDATE()
{
	set_permission
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
