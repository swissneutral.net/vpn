#!/bin/bash

##################################################
# Service - locales
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
	:
}

POSTINSTALL()
{
	# Remove this line when implemented
	echo "LOCALES CONFIGURATIONS"
	echo "----------------------"
	echo "You probably need to select the numer '158 230'"
    dpkg-reconfigure locales
    locale-gen
}


####################
# Update
####################

PREUPDATE()
{
    :
}

POSTUPDATE()
{
    locale-gen
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Nothing to do"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Nothing to do"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
