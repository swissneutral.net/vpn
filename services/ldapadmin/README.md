Descriptif du service
=====================

Service deployant une interface web d'administatration de LDAP.

Info particulière déployement
-----------------------------

Testé sur debian Jessie. D'après certains tests l'installation sur debian stretch ne fonctionne pas.

Il peut aussi être envisageable d'installer ce service sur yunohost.


Cas de test après dépoyement
----------------------------

- Test de l'accès à l'interface.
- Tester connexion et authentification au serveur LDAP.

TODO
----

- Voir si installable sur Yunohost 3.0 (debian stretch) :
    - Si oui -> Abandonner ce service
    - Si non -> Tester install sur debian stretch vierge
        - Si possible + temps à disposition -> Debug afin que ce service soie installable sur debian stretch.
- Tests