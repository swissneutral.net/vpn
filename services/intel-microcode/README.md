Descriptif du service
=====================

Installe un paquet qui permet de mettre à jours le microcode des CPU.


Info particulière déploiement
-----------------------------

A installer uniquement sur les hyperviseurs.

Cas de test après dépoyement
----------------------------

Vérifier que le numéro de firmware change après un reboot (après une mise à jours du firmware).
On peut utiliser la commande `grep microcode /proc/cpuinfo` pour voir la version du firmware du CPU.
