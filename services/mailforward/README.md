Descriptif du service
=====================

Service permettant de configurer la VM afin que tous les mail soient forwardé à la bonne adresse mail.

Info particulière déployement
-----------------------------

### Mauvais hostname lors de l'envois de mail

Il semblerait qu'il persiste un bug documenté ici : https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=214741

Le problème étant que postfix de tiens pas compte de ce que retourne la commande `hostname -f`, à la place il utilise `localdomain`. On peut observer le bug avec la commande `postconf`. Voici l'extrait du retours problématique :
```
...
mydomain = localdomain
myhostname = ns1.localdomain
mynetworks = 127.0.0.0/8
mynetworks_style = ${{$compatibility_level} < {2} ? {subnet} : {host}}
myorigin = $myhostname
...
```

Provisoirement afin de "bypasser" ce bug on peut simplement ajouter la ligne suivante (par exemple) dans `/etc/postfix/main.cf`:
```
myhostname = ns1.swissneutral.net
```

### Authorisation d'envois de mail depuis les VM

Dû à la config `spf` les VM n'ont pas officiellement le droit d'envoyer des mail.
Afin que notre serveur mail authorise quand même la reception de mail la configuration postfix de notre serveur Yunohost (`apps.swissneutral.net`) devra être modifié comme suit:

Fichier `/etc/postfix/main.cf`:

```
-mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
+mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 185.250.56.0/27 10.10.10.0/24 [2a0a:db40::]/64 [2a0a:db40:0:1::]/64
```


Cas de test après dépoyement
----------------------------

- Déployer le service
- Tester en envoyant un mail à root@localhost (exemple `echo "test forward" | mail root@localhost`) et constater que le mail fini à l'addresse email définie `ADMIN_EMAIL`.
