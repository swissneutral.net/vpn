Descriptif du service
=====================

Met en place un serveur SSH avec les derniers standards de sécurité

Info particulière déployement
-----------------------------

### Tunneling SSH

Lorsque ce service à pour but de faire office de tunnel il est indispensable d'activer le tunneling dans le container en question. Il faudra activer le tunneling dans ssh en activant configurant `SSH_TUNNEL_ALLOWED="point-to-point"`. D'autre part il est nécessaire d'ajouter les lignes suivante dans le fichier `/etc/pve/lxc/no_conainter.conf`

```
lxc.cgroup2.devices.allow: c 10:200 rwm
lxc.mount.entry: /dev/net dev/net none bind,create=dir
```

#### Mise en place Tunnel SSH

Le but est de créer un subnet /126 par client. On aura donc un tunnel P2P SSH.

Dans notre cas on peut utiliser la commande suivante afin d'établir un tunnel vers l'infrastrcture :

```
n=1; sudo ssh \
  -o PermitLocalCommand=yes \
  -i /home/user/.ssh/clef_privee \
  -o LocalCommand="sudo ip -6 address add VPN_SUBNET_PREFIX${n}2/126 dev tun$n; sudo ip link set tun$n up; \
    sudo ip route add SUBNET_1/64 via VPN_SUBNET_PREFIX${n}1; \
    sudo ip route add SUBNET_2/64 via VPN_SUBNET_PREFIX${n}1" \
  -o ServerAliveInterval=60 \
  -w $n:$n -C \
  root@ADRESSE_SERVEUR -p PORT \
  "ip address add VPN_SUBNET_PREFIX${n}1/126 dev tun$n; ip link set tun$n up; echo tun$n ready"
```

La valeur du `n` doit être unique pour chaque connexion (a donc incrémenter lorsque il y a plusieurs connexion en même temps).

La valeur de `VPN_SUBNET_PREFIX` est le prefix du subet consacrét au montage des tunnels.

`SUBNET_1` et `SUBNET_2` sont les adresse des subnets auquels on veux accéder via le VPN.

Cas de test après dépoyement
----------------------------

- Tester la connexion au serveur SSH.
- Tester l'authentification par clef public/privée
- Vérifier que l'authentification par mot de passe soie DÉSACTIVÉE.
- Vérifier que le service tourne sur un port différents du port 22.
- Valider le niveau de sécurité ici : https://tls.imirhil.fr

TODO
----

- Tests
