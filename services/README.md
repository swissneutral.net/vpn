Définition du formatage des scripts
===================================

Afin d'avoir des scripts uniformes et facilement compréhensible il a été décidé de définire un formatage précis.


## Structure de base

Le répertoire services contient tous les services qui pourront être installer sur les machines virtuelles. Chaque service se trouve dans un dossier nommé de la manière la plus simple possible.

Chaque dossier de service inclut :

- Un répertoire nomé "system_root" contenant les fichiers à copier par le script d'installation dans la racine du système cible.

- Un répertoire facultatif nomé "data" contenant des fichiers nécéssaires au script d'installation du service mais n'ayant pas besoin d'être copié dans la racine du système cible.

- Un script nommé au format "service_name.sh" qui sera appelé par le script "main.sh"


Le dossier exemple peut être copié comme base pour les nouveaux services.


--------------------

Chaque script de service permettera l'installation, l'upgrade, et la restauration pour chaques vm du dit service. Chaque script devra contenir au minimum les fonctions suivantes : 

- PREINSTALL : Ce script sera chargé de réinstaller les éléments nécessaires au fonctionnement du service sur un container ou VM vierge. Ce script sera executé avant la copie des fichiers issus de root_no-vm. Généralement il procédera à l'installation des paquets debian par exemple. 

- POSTINSTALL : Ce script sera chargé de réinstaller les éléments nécessaire au fonctionnement du service sur un container ou VM vierge. Ce script sera executé après la copie des fichiers. Généralement il procédera à la configuration des services installés par le script PREINSTALL.

--------------------

- PREUPDATE : Ce script permettra de mettre à jour la configuration du service sur un container ou une VM sur laquelle le service a déjà été installé. Ce script sera executé avant la copie des fichiers.

- POSTUPDATE :  Ce script permettra de mettre à jour la configuration du service sur un container ou une VM sur laquelle le service à déjà été installé. Ce script sera executé après la copie des fichiers.

--------------------

- BACKUP : Ce script permet de sauvegarder tous les fichiers nécessaires à la restauration entière du service.

- RESTORE : Ce script permet de restaurer toute les données sauvegardées. Ce script sera exectué après l'installation du service. Ce script ne procédera donc pas à l'installation du service mais uniquement à la restauration des données.

--------------------

Attention : même si dans le cas d'un service la fonction n'est pas utile il est important de l'implémententer sous forme vide de manière que le script principal puisse appeller ces fonctions pour tous les services.

L'idée est que tous les scripts ne contiennent pas de configuration dur. Tous les paramètres de configuration devront être définit en tant que variables d'environnement (voir "Type de variables et noms") et ces paramètre seront définis dans le fichier config.sh. D'autres paramètres qui seront en lien avec la sécurité tel que le port ssh ou des passwords seront stockés eux dans private.sh.


## Type de variables et noms

Dans les scripts il y aura généralement deux grands types de variables : 

- Les variables d'environnement qui seront issuent de config.sh et private.sh. Ces variables posséderont des noms entièrement en MAJUSCULES. Si la variable contient plusieurs mots, les mots seront séparés par le caractère '&#95;'. Exemple : "DNS_SERVER_1_NAME"

- Les variables locales seront utilisées uniquement pour le script en question et seront écrites en minuscules. Si la variable contient plusieurs mots, les mots seront séparés par le caractère '&#95;'. Exemple : "vpn_conf_list".


## Fonctions

L'idée est la même que pour la définition des noms de variables. On a aussi deux types de fonctions : 

- Les fonctions appellées par le script principal (main.sh). Soit PREINSTALL, POSTINSTALL, PREUPDATE, POSTUPDATE, BACKUP, RESTORE. Elles auront un nom en MAJUSCULES.

- Les fonctions locales. S'il est nécessaire de définire des fonctions locales, on les distinguera des fonctions appellée par main.sh grâce à un nom en minuscule. Soit par exemple : update_vpn_config


## Remplacement de valeur dans les fichiers de configurations

Comme expliqué plus haut, le but est d'avoir quelque chose de générique qui puisse être flexible dans le sens où toute chose qui peut être changée en fonction de l'infrastrucutre ou de la structure réseau doit être définie uniquement dans config.sh ou private.sh. Certains fichier de configuration des services peuvent contenir de tels paramètres.

Le script main.sh s'occupe de trouver tous les nom de variables globales qui sont définies dans les fichiers config.sh et private.sh. Lors de l'installation des services, au démarage d'une action INSTALL ou UPDATE, le script main.sh effectue les actions suivantes :

- Création d'un répertoire temportaire "tmp/"

- Copie de tous le répertoire "services" dans le répertoire temporaire

- Remplacement automatique de toutes les variables globales de tous les fichiers du répertoire temporaire. Exemple : le texte &#95;&#95;SSH_PORT&#95;&#95; dans le fichier sshd_conf sera templacé par la valeur inscrite dans le fichier private.conf soit 22. Ceci concerne autant les répertoirs data/ que system_root, que les script d'installation.

- Entre les action PREINSTALL et POSTINSTALL ou PREUPDATE et POSTUPDATE : Copie des dossiers system_root des services à installer vers la racine du serveur

- Effacement du dossier temporaire à la fin du processus d'installation ou d'update.


Lors de l'écriture des fichiers de configuration des services, pour utiliser des variables globales vous devez :

- Définir une variable globale dans le fichier config.sh ou private.sh. Exemple : VPN_SERVER_IP6="2001::1234"

- Ajouter le nom de la variable dans les fichiers de configurations précédée et succédée par __ . Exemple : &#95;&#95;VPN_SERVER_IP6&#95;&#95;


Exemple : Nous souhaitons avoir une config dynamique pour le fichier service/vpn/etc/vpn/vpn_conf_1.conf


Fichier config.sh :

```
VPN_SERVER_IP6="2001::1234"
DNS_SERVER_1_IP4="127.0.0.1"
```

Fichier service/vpn/etc/vpn/vpn_conf_1.conf (extrait) :

```
verb 3
status /var/log/openvpn/swissneutral-vpn1.log
log-append /var/log/openvpn/swissneutral-vpn1.log

push "redirect-gateway"
push "route-ipv6 ::/0 __VPN_SERVER_IP6__"
push "dhcp-option DNS __DNS_SERVER_1_IP4__"
```


Les textes "&#95;&#95;VPN_SERVER_IP6&#95;&#95;" et "&#95;&#95;DNS_SERVER_1_IP4&#95;&#95;" seront automatiquement remplacés par les valeurs présentes dans config.sh ou private.sh.


Exemple après exécution du script main.sh :

```
verb 3
status /var/log/openvpn/swissneutral-vpn1.log
log-append /var/log/openvpn/swissneutral-vpn1.log

push "redirect-gateway"
push "route-ipv6 ::/0 2001::1234"
push "dhcp-option DNS 127.0.0.1"
```

Pour garder une bonne lisiblité du code et bonne uniformité il est important d'ajouter l'élément '__' avant et après le nom de la valeur à changer (cela évite un conflit avec un élément de ce nom dans le fichier de configuration). D'autre part, garder le nom de la variable pour le nom de l'élément à remplacer est certainement une bonne pratique.

