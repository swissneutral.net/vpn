Descriptif du service
=====================

Met en place un résolveur DNS. Pour cela nous utilisons le logiciel unbound.


Info particulière déployement
-----------------------------




Cas de test après dépoyement
----------------------------

- Tester la résolution DNS de différents domaines.
- Tester la résolution DNS de domaines équipé de DNSSEC.
- Tester la résolution DNS sur TLS.

TODO
----

- Mise à jours de sécurité automatique
- Tests

TODO
----

Voir Fichier CONTRIBUTING
