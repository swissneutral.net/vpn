#!/bin/bash

##################################################
# Service - firewall
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
    :
}

POSTINSTALL()
{
    chmod 700 /etc/init.d/iptables
    chmod 700 /etc/init.d/ip6tables
    systemctl enable iptables
    systemctl enable ip6tables
    systemctl restart iptables
    systemctl restart ip6tables
}


####################
# Update
####################

PREUPDATE()
{
	:
}

POSTUPDATE()
{
    systemctl daemon-reload
    systemctl restart iptables
    systemctl restart ip6tables
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Nothing to do  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Nothing to do $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
