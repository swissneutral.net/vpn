Descriptif du service
=====================

Serveur DNS autoritaire master pour les zones de l'association. Le serveurs utilisé sera bind9.

Procédure installation
----------------------

Après installation du service il est nécessaire de configurer les identifiant d'accès à la base de donnée coin.

Pour se connecter à postgresql utiliser la commande : `sudo -u postgres psql -d coin`:

Commandes création de d’utilisateurs avec les permissions nécessaires:
```
CREATE USER bind_access_info WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    NOINHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    PASSWORD 'THE BEST PASSWORD';
GRANT CONNECT ON DATABASE coin TO bind_access_info;
GRANT SELECT ON TABLE public.reverse_dns_reversednsentry,
                      public.resources_ipsubnet_name_server,
                      public.reverse_dns_nameserver,
                      public.configuration_configuration,
                      public.resources_ipsubnet,
                      public.offers_offersubscription,
                      public.members_member
TO bind_access_info;
```

Il est ensuite nécessaire d'ajouter la ligne suivante dans le fichier `/etc/postgresql/11/main/pg_hba.conf` :
```
host coin bind_access_info fd0a:db40:0:1::c/128 password
```

D'autre part Postgresql doit écouter sur le LAN IPv6 nécessaire. Éditer ainsi la ligne du fichier `/etc/postgresql/11/main/postgresql.conf` :
```
port = 5432 # Parfois c'est mis à 5433
listen_addresses = 'localhost, fd0a:db40:0:1::4'
```

Créer un fichier dans `/etc/coin-sync-access.yml` et lui mettre les donnée suivantes :
```yml
db_address :  "1.2.3.4"
db_name :     "coin"
db_username : "bind_access_info"
db_password : "THE BEST PASSWORD"
```

Ensuite il faut mettre des permission restrictives sur ce fichier !!


Cas de test après déploiement
----------------------------

- Tester la résolution DNS pour les domaine de l'association.
    - Tester tous les types d'enregistrement DNS (A, AAAA, PTR, MX, TXT (srv), DNSSEC)
- Tester DNSSEC.
- Tester la synchronisation entre serveur master et slave.
- Tester la synchronisation avec coin :
    - Valider que les reverse DNS dans coin soient ajouté dans les zones de Bind.
        - Tester avec plusieurs styles de reverse PTR (particulièrement en IPv6). Par exemple avec un /128 ou /64.
    - Valider que un changement dans coin génère une mise à jours de la zone en question.
    - Valider que les zones ne subissent pas de mise à jours perpétuelles alors qu'il n'y pas de changement (pourrait être du au cron qui appelle coin-sync régulièrement). Pour valider cela normalement un appel de la commande `coin-sync` à plusieurs reprise ne doit pas à chaque fois régénérer la configuration des zones.
    - Valider que la délégation de zone reverse DNS depuis coin soie propagé correctement dans les zone de Bind.

TODO
----

Voir Fichier CONTRIBUTING
