#[macro_use]
extern crate json;
use std::env;
use std::io::prelude::*;
use std::io::{BufReader, Write};
use std::os::unix::net::UnixStream;
use std::process;

// const SOCKET_PATH: &'static str = "/home/coin_request/daemon.socket"; // For debug
const SOCKET_PATH: &'static str = "/daemon.socket";
const DEBUG_MODE: bool = true;

fn main() {
    let mut args = env::args();
    let mut user_name = match args.nth(1) {
        Some(u) => u,
        None => {
            exit_properly("Can't get argument".to_owned());
            "".to_owned()
        }
    };
    // use the second argument when we use ssh connexion
    if user_name == "-c" {
        user_name = match args.next() {
            Some(u) => u,
            None => {
                exit_properly("Can't get second argument".to_owned());
                "".to_owned()
            }
        };
    }
    // Connect to daemon
    let mut socket = match UnixStream::connect(SOCKET_PATH.to_owned()) {
        Ok(sock) => sock,
        Err(_) => {
            exit_properly("Can't connect to daemon".to_owned());
            return;
        }
    };
    // Send request
    match socket.write_all(
        object! {
            "request_type" => "get_cert",
            "user_name" => user_name,
        }
        .dump()
        .as_bytes(),
    ) {
        Ok(_) => {}
        Err(_) => {
            exit_properly("Can't send request to deamon".to_owned());
        }
    };
    match socket.write_all("\n".as_bytes()) {
        Ok(_) => {}
        Err(_) => {
            exit_properly("Can't send request to deamon".to_owned());
        }
    };
    // Read result
    let mut result = String::new();
    let mut buf = BufReader::new(socket);
    match buf.read_line(&mut result) {
        Ok(_) => {}
        Err(_) => {
            exit_properly("Can't read answer from deamon".to_owned());
        }
    };
    let result_json = match json::parse(&result) {
        Ok(r) => r,
        Err(_) => {
            exit_properly("Can't decode answer's daemon".to_owned());
            return;
        }
    };
    // Show result
    if result_json["build_passed"] == true {
        println!("{}", result);
    } else {
        exit_properly(
            "Build failed to build. Daemon error reported :".to_owned()
                + &result_json["error"].to_string(),
        );
    }
}

fn exit_properly(error: String) {
    let r;
    if DEBUG_MODE {
        r = object! {
            "build_passed" => false,
            "error" => error,
        };
    } else {
        r = object! {
            "build_passed" => false,
        };
    }
    println!("{:#}", r);
    process::exit(1);
}
