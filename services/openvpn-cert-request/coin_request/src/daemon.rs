/*
[package]
name = "build_client_cert"
version = "0.1.1"
authors = ["Josué Tille <josue@tille.ch>"]

[dependencies]
json = "0.11.13"
regex = "1.1.6"
rexpect = "0.3.0"
users = "0.9.1"
libc = "0.2.58"
rpassword = "3.0.2"
sendmail = "2.0.0"

[[bin]]
name = "deamon"
path = "src/daemon.rs"

[[bin]]
name = "client_request"
path = "src/client_request.rs"

[[bin]]
name = "client_register"
path = "src/client_register.rs"
*/
///////////////////////////////////////////////////////////////////////////////////////////

// How to build :
// - Install cargo if not done by : sudo apt install cargo
// - Make project : cargo init
// - Copy the toml file (at the to of this file) in : Cargo.toml
// - Add this file in : src/main.rs
// - Build debug version :  cargo build
// - Build final version : cargo build --release
// - You will find the binary in : target/debug/build_client_cert and target/release/build_client_cert

///////////////////////////////////////////////////////////////////////////////////////////

#[macro_use]
extern crate json;
extern crate libc;
extern crate regex;
extern crate rexpect;
extern crate sendmail;
extern crate users;

use std::{
    env,
    ffi::CString,
    fs,
    fs::File,
    io::{
        prelude::*,
        {BufReader, Write},
    },
    net::Shutdown,
    os::unix::{
        net::{UnixListener, UnixStream},
        process::CommandExt,
    },
    process::{Command, Stdio},
    str,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
    time::Duration,
};

use regex::Regex;

use libc::{chmod, chown};
use users::get_user_by_name;

use sendmail::email;

const ROOT_CA_PATH: &'static str = "/root/openvpnCA";
const EASYRSA_SCRIPT: &'static str = "/root/openvpnCA/easyrsa";
const SOCKET_PATH: &'static str = "/home/coin_request/daemon.socket";
const SOCKET_USER_OWNER: &'static str = "coin_request";
const TEST_PWD_USER: &'static str = "test_pwd_user";
const SOCKET_GID: u32 = 0;
const EXPECT_SCRIPT_UID: u32 = 0;
const CERT_SIZE: usize = 4000;
const TA_SIZE: usize = 1000;
const WAIT_SEND_EMAIL_TIME: u64 = 5 * 60; // 5 min
const MAIL_REQUEST_PWD_CONTENT: &str =
"Hello,\n
\n
It look like that you have restarted the cert-request deamon but you haven't registred the password. You should probably do this !!!\n
\n
See you soon :-)\n";
const MAIL_REQUEST_PWD_SRC_ADDR: &'static str = "root@__VPN_CERT_REQUEST_FQDN__";
const MAIL_REQUEST_PWD_DST_ADDR: &'static str = "__ADMIN_EMAIL__";
const MAIL_REQUEST_PWD_SUBJECT: &'static str =
    "[Infra][VPN Certs] Cert request manager deamon need a password";

struct DaemonConfig {
    debug_mode: bool,
    manual_password: bool,
    password_is_defined: Arc<AtomicBool>,
    password_pwd: String,
}

impl DaemonConfig {
    fn set_password(&mut self, new_password: String) {
        self.password_is_defined.store(true, Ordering::Relaxed);
        self.password_pwd = new_password.to_owned();
    }
    fn clean_password(&mut self) {
        self.password_is_defined.store(false, Ordering::Relaxed);
        self.password_pwd = "".to_owned();
    }
}

fn main() {
    // Remove old socket if exit
    match fs::remove_file(SOCKET_PATH.to_owned()) {
        Ok(_) => {}
        Err(_) => {}
    }

    let mut debug_mode = false;
    let mut manual_password = false;

    // Enable debug if set
    for argument in env::args() {
        match argument.as_str() {
            "debug" => {
                debug_mode = true;
            }
            "man_pwd" => {
                manual_password = true;
            }
            "help" => {
                println!("Option :");
                println!("debug : debug mode");
                println!("man_pwd : give the possiblity to see all interaction with easyrsa");
                return;
            }
            _ => {}
        }
    }

    // Create ca_password struct to be able to store the password
    let mut daemon_config = DaemonConfig {
        debug_mode: debug_mode,
        manual_password: manual_password,
        password_is_defined: Arc::new(AtomicBool::new(false)),
        password_pwd: String::new(),
    };

    // Init socket
    let listener = match UnixListener::bind(SOCKET_PATH.to_owned()) {
        Ok(sock) => sock,
        Err(e) => {
            println!(
                "Couldn't create socket at path {} : {:?}",
                SOCKET_PATH.to_owned(),
                e
            );
            return;
        }
    };
    // Define permission and owner for socket
    let socket_path = CString::new(SOCKET_PATH)
        .expect(&("Can't create a string for ".to_owned() + &SOCKET_PATH.to_owned()));
    match get_user_by_name(SOCKET_USER_OWNER) {
        Some(user) => unsafe {
            if chown(socket_path.as_ptr(), user.uid(), SOCKET_GID) != 0 {
                println!(
                    "Couldn't change owner for socket at path {}",
                    SOCKET_PATH.to_owned()
                );
            }
            if chmod(socket_path.as_ptr(), 0o600) != 0 {
                println!(
                    "Couldn't change permission for socket at path {}",
                    SOCKET_PATH.to_owned()
                );
            }
        },
        None => {
            println!("Couldn't get user uid for {}", SOCKET_USER_OWNER);
        }
    }

    let password_defined = daemon_config.password_is_defined.clone();
    thread::spawn(move || send_mail_if_no_password_registred(password_defined));

    // Main loop to get all request
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                handle_client(stream, &mut daemon_config);
            }
            Err(err) => {
                println!("Can't get incomming connexion : {}", err);
            }
        }
    }
}

fn handle_client(mut stream: UnixStream, daemon_config: &mut DaemonConfig) {
    // Create copy of main buffer
    let mut buf = BufReader::new(match stream.try_clone() {
        Ok(b) => b,
        Err(e) => {
            operation_failled(
                stream,
                daemon_config,
                "Can't create a buffer. Erreur : ".to_owned() + &e.to_string(),
            );
            return;
        }
    });
    let stream2 = match stream.try_clone() {
        Ok(b) => b,
        Err(e) => {
            operation_failled(
                stream,
                daemon_config,
                "Can't create a buffer. Erreur : ".to_owned() + &e.to_string(),
            );
            return;
        }
    };
    // Get request
    let mut request = String::new();
    match buf.read_line(&mut request) {
        Ok(_) => {}
        Err(e) => {
            operation_failled(
                stream,
                daemon_config,
                "Can't read request from client. Erreur : ".to_owned() + &e.to_string(),
            );
            return;
        }
    }
    let request = match json::parse(&request) {
        Ok(r) => r,
        Err(e) => {
            operation_failled(
                stream,
                daemon_config,
                "Can't decode request. Erreur : ".to_owned() + &e.to_string(),
            );
            return;
        }
    };
    if (!daemon_config.password_is_defined.load(Ordering::Relaxed))
        && (request["request_type"] == "set_password")
    {
        // Save password
        if request["password"].to_string().len() > 4 {
            daemon_config.set_password(request["password"].to_string());

            // Know if already in CA database
            let do_renew_cert = match is_user_in_ca(
                ROOT_CA_PATH.to_owned() + "/pki/index.txt",
                TEST_PWD_USER.to_owned(),
            ) {
                Ok(r) => r,
                Err(_) => {
                    operation_failled(
                        stream,
                        daemon_config,
                        "Can't know if user is in CA database".to_owned(),
                    );
                    return;
                }
            };
            match ensure_renewed_issued_file_dont_exist(TEST_PWD_USER.to_owned()) {
                Ok(_) => {}
                Err(_) => {
                    operation_failled(
                        stream,
                        daemon_config,
                        "Cannot clean file before cert generation".to_owned(),
                    );
                    return;
                }
            }
            match call_easy_rsa(TEST_PWD_USER.to_owned(), do_renew_cert, daemon_config) {
                Ok(_) => {}
                Err(e) => {
                    daemon_config.clean_password();
                    operation_failled(
                        stream,
                        daemon_config,
                        "Subscript failed. Probably bad password. Error : ".to_owned()
                            + &e.to_string(),
                    );
                    return;
                }
            }
        } else {
            operation_failled(stream, daemon_config, "Password too shord".to_owned());
            return;
        }
        match stream.write_all(
            object! {
                "op_success" => true,
            }
            .dump()
            .as_bytes(),
        ) {
            Ok(_) => {}
            Err(e) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string(),
                );
                return;
            }
        };
        match stream.write_all("\n".as_bytes()) {
            Ok(_) => {}
            Err(e) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string(),
                );
                return;
            }
        };
    } else if (daemon_config.password_is_defined.load(Ordering::Relaxed))
        && (request["request_type"] == "get_cert")
    {
        // Filter username
        let user_name = request["user_name"].to_string();
        if !Regex::new(r"^[\w\n\-]+$").unwrap().is_match(&user_name) {
            operation_failled(stream, daemon_config, "username not valid".to_owned());
            return;
        }

        let cert_file_path = [
            "/pki/issued/".to_owned() + &user_name + ".crt",
            "/pki/private/".to_owned() + &user_name + ".key",
            "/server_info/ca.crt".to_owned(),
            "/server_info/ta.key".to_owned(),
        ];

        // Know if already in CA database
        let do_renew_cert = match is_user_in_ca(
            ROOT_CA_PATH.to_owned() + "/pki/index.txt",
            user_name.to_owned(),
        ) {
            Ok(r) => r,
            Err(_) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Can't know if user is in CA database".to_owned(),
                );
                return;
            }
        };

        // Clean cert file before generation if needed
        match ensure_renewed_issued_file_dont_exist(user_name.to_owned()) {
            Ok(_) => {}
            Err(_) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Cannot clean file before cert generation".to_owned(),
                );
                return;
            }
        }

        // Launch the sub command
        match call_easy_rsa(user_name.to_owned(), do_renew_cert, daemon_config) {
            Ok(r) => match r {
                Ok(_) => {}
                Err(e) => {
                    operation_failled(
                        stream,
                        daemon_config,
                        "Subscript failed. Error : ".to_owned() + &e.to_string(),
                    );
                    return;
                }
            },
            Err(e) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Subscript failed. Error : ".to_owned() + &e.to_string(),
                );
                return;
            }
        }
        // Read all cert file
        let mut result = [
            String::with_capacity(CERT_SIZE),
            String::with_capacity(CERT_SIZE),
            String::with_capacity(CERT_SIZE),
            String::with_capacity(TA_SIZE),
        ];
        for i in 0..cert_file_path.len() {
            let file_path = ROOT_CA_PATH.to_owned() + &cert_file_path[i];
            match get_file_cnt(file_path.to_owned(), &mut result[i]) {
                Ok(_) => {}
                Err(_) => {
                    operation_failled(
                        stream,
                        daemon_config,
                        "Can't read file : ".to_owned() + &file_path,
                    );
                    return;
                }
            }
        }
        // Show result as json
        let r = object! {
            "build_passed" => true,
            "crt" => result[0].to_owned(),
            "key" => result[1].to_owned(),
            "ca" => result[2].to_owned(),
            "ta" => result[3].to_owned(),
        };
        match stream.write_all(r.dump().as_bytes()) {
            Ok(_) => {}
            Err(e) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string(),
                );
                return;
            }
        };
        match stream.write_all("\n".as_bytes()) {
            Ok(_) => {}
            Err(e) => {
                operation_failled(
                    stream,
                    daemon_config,
                    "Cant't write answer to client. Erreur : ".to_owned() + &e.to_string(),
                );
                return;
            }
        };
    } else {
        operation_failled(
            stream,
            daemon_config,
            "Deamon not ready for this action".to_owned(),
        );
    }
    stream2
        .shutdown(Shutdown::Both)
        .expect("shutdown stream failed");
}

fn call_easy_rsa(
    user_name: String,
    do_renew_cert: bool,
    daemon_config: &mut DaemonConfig,
) -> Result<Result<(), String>, rexpect::error::Error> {
    let mut c = Command::new(EASYRSA_SCRIPT.to_owned());
    let easyrsa_cmd = match do_renew_cert {
        false => std::collections::LinkedList::from(["build-client-full", &user_name, "nopass"]),
        true => std::collections::LinkedList::from(["renew", &user_name]),
    };
    c.args(easyrsa_cmd);
    c.current_dir(&ROOT_CA_PATH);
    c.uid(EXPECT_SCRIPT_UID);
    if daemon_config.manual_password {
        c.stdout(Stdio::inherit());
        c.stdin(Stdio::inherit());
        c.stderr(Stdio::inherit());
        c.status().expect("Failed to execute command");
    } else {
        let mut p = rexpect::session::spawn_command(c, Some(8000))?;
        p.exp_regex(
            &("Enter pass phrase for ".to_owned() + ROOT_CA_PATH + "/pki/private/ca.key:"),
        )?;
        p.send_line(&daemon_config.password_pwd)?;
        let result = p.exp_eof()?;
        if !result.contains(&"Database updated")
            || !result.contains(&"Write out database with 1 new entries")
            || !result.contains(&"Certificate created at")
        {
            return Ok(Err(
                "Build process didn't generated a new certificate, output : ".to_owned() + &result,
            ));
        }
        match p
            .process
            .wait()
            .expect("Can't get result status of child process")
        {
            rexpect::process::wait::WaitStatus::Exited(_pid, status) => {
                if status != 0 {
                    return Ok(Err(
                        "Build process failed with error :".to_owned() + &status.to_string()
                    ));
                }
            }
            _ => {
                return Ok(Err("Unknown state of process".to_string()));
            }
        }
    }
    // Now if it's a renew, we should revoke the previous cert to ensure that we won't have any conflict the next time
    if do_renew_cert {
        c = Command::new(EASYRSA_SCRIPT.to_owned());
        c.args(["revoke-renewed", &user_name]);
        c.current_dir(&ROOT_CA_PATH);
        c.uid(EXPECT_SCRIPT_UID);
        if daemon_config.manual_password {
            c.stdout(Stdio::inherit());
            c.stdin(Stdio::inherit());
            c.stderr(Stdio::inherit());
            c.status().expect("Failed to execute command");
        } else {
            let mut p = rexpect::session::spawn_command(c, Some(8000))?;
            p.exp_regex(
                &("Enter pass phrase for ".to_owned() + ROOT_CA_PATH + "/pki/private/ca.key:"),
            )?;
            p.send_line(&daemon_config.password_pwd)?;
            let result = p.exp_eof()?;
            if !result.contains(&"Revocation was successful") {
                return Ok(Err("Revocation didn't work correctly, output : "
                    .to_owned()
                    + &result));
            }
            match p
                .process
                .wait()
                .expect("Can't get result status of child process")
            {
                rexpect::process::wait::WaitStatus::Exited(_pid, status) => {
                    if status != 0 {
                        return Ok(Err("Revocation process failed with error :".to_owned()
                            + &status.to_string()));
                    }
                }
                _ => {
                    return Ok(Err("Unknown state of process".to_string()));
                }
            }
        }
    }
    Ok(Ok(()))
}

fn ensure_renewed_issued_file_dont_exist(user_name: String) -> std::io::Result<()> {
    let file_path = ROOT_CA_PATH.to_owned() + "/pki/renewed/issued/" + &user_name + ".crt";
    if std::path::Path::new(&file_path).exists() {
        match std::fs::remove_file(file_path) {
            Ok(_) => {}
            Err(e) => return Err(e),
        }
    }
    return Ok(());
}

fn get_file_cnt(path: String, result: &mut String) -> std::io::Result<()> {
    let mut file = File::open(path)?;
    file.read_to_string(result)?;
    Ok(())
}

fn is_user_in_ca(file_path: String, user_name: String) -> std::io::Result<bool> {
    let file = File::open(file_path.to_owned())?;
    let file = BufReader::new(file);
    for line in file.lines() {
        let l = line.expect(&("Can't read line in file :".to_owned() + &file_path.to_owned()));
        if l.find(&user_name) != None {
            return Ok(true);
        }
    }
    return Ok(false);
}

fn operation_failled(mut stream: UnixStream, daemon_config: &DaemonConfig, error: String) {
    println!("{}", error);
    let r;
    if daemon_config.debug_mode {
        r = object! {
            "op_success" => false,
            "build_passed" => false,
            "error" => error,
        };
    } else {
        r = object! {
            "op_success" => false,
            "build_passed" => false,
        };
    }
    match stream.write_all(r.dump().as_bytes()) {
        Ok(_) => {}
        Err(e) => {
            println!("Cant't write answer to client. Error : {}", e);
            stream
                .shutdown(Shutdown::Both)
                .expect("shutdown stream failed");
            return;
        }
    };
    match stream.write_all("\n".as_bytes()) {
        Ok(_) => {}
        Err(e) => {
            println!("Cant't write answer to client. error : {}", e);
            stream
                .shutdown(Shutdown::Both)
                .expect("shutdown stream failed");
            return;
        }
    };
    stream
        .shutdown(Shutdown::Both)
        .expect("shutdown stream failed");
}

fn send_mail_if_no_password_registred(password_is_defined: Arc<AtomicBool>) {
    thread::sleep(Duration::from_secs(WAIT_SEND_EMAIL_TIME));
    if !password_is_defined.load(Ordering::Relaxed) {
        match email::send(
            MAIL_REQUEST_PWD_SRC_ADDR,
            &[MAIL_REQUEST_PWD_DST_ADDR],
            MAIL_REQUEST_PWD_SUBJECT,
            MAIL_REQUEST_PWD_CONTENT,
        ) {
            Ok(_) => {}
            Err(e) => {
                println!("Can't send email to request password. Error : {}", e);
            }
        };
    }
}
