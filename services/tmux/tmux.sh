#!/bin/bash

##################################################
# Service - exemple
#
# This is an exemple of service script file
##################################################

####################
# Init
####################

# Exit if error
set -eu

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
data_folder=$(dirname $(readlink -f $0))/data

####################
# Shared functions
####################


####################
# Install
####################

PREINSTALL()
{
    apt-get install -y tmux git
    (
        cd /root
        git clone https://github.com/gpakosz/.tmux.git
        ln -s -f .tmux/.tmux.conf
        cp .tmux/.tmux.conf.local .
    )
}

POSTINSTALL()
{
    mkdir -p /root/tmux/nano
    grep -q "bash_aliases" /root/.bashrc || echo '. ~/.bash_aliases' >> /root/.bashrc
}


####################
# Update
####################

PREUPDATE()
{
    apt-get install -y tmux
    [[ -e /root/.tmux ]] && rm -r /root/.tmux
    (
        cd /root
        git clone https://github.com/gpakosz/.tmux.git
        ln -s -f .tmux/.tmux.conf
        cp .tmux/.tmux.conf.local .
    )
}

POSTUPDATE()
{
    mkdir -p /root/tmux/nano
    grep -q "bash_aliases" /root/.bashrc || echo '. ~/.bash_aliases' >> /root/.bashrc
}


####################
# Backup / Restore
####################

BACKUP()
{
    # Get backup path from main.sh
    backup_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
    # Get restore path from main.sh
    restore_path=$1

    # Remove this line when implemented
    echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
    POSTINSTALL
    ;;
    PREUPDATE)
    PREUPDATE
    ;;
    POSTUPDATE)
    POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
    echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
