Descriptif du service
=====================

Service permettant de mettre en place un firewall d'entrée sur toute l'infrastructure.


Info particulière déployement
-----------------------------



### Accès aux donnée de coin

Après installation du service il est nécessaire de configurer les identifiant d'accès à la base de donnée coin.

Pour se connecter à postgresql utiliser la commande : `sudo -u postgres psql -d coin`:

Commandes création de l'utilisateurs avec les permissions nécessaires:
```
CREATE USER firewall_access_info WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    NOINHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    PASSWORD 'THE BEST PASSWORD';
GRANT CONNECT ON DATABASE coin TO firewall_access_info;
GRANT SELECT ON TABLE public.vpn_vpnconfiguration,
                      public.configuration_configuration,
                      public.resources_ipsubnet,
                      public.offers_offersubscription,
                      public.members_member
TO firewall_access_info;
```

Il est ensuite nécessaire d'ajouter la ligne suivante dans le fichier `/etc/postgresql/11/main/pg_hba.conf` :
```
host coin firewall_access_info fd0a:db40:0:1::1/128 password
```

D'autre part Postgresql doit écouter sur le LAN IPv6 nécessaire. Editer ainsi la ligne du fichier `/etc/postgresql/11/main/postgresql.conf` :
```
port = 5432 # Parfois c'est mis à 5433
listen_addresses = 'localhost, fd0a:db40:0:1::4'
```

Créer un fichier dans `/etc/coin-sync-access.yml` et lui mettre les donnée suivantes :
```yml
db_address :  "1.2.3.4"
db_name :     "coin"
db_username : "firewall_access_info"
db_password : "THE BEST PASSWORD"
```

Ensuite il faut mettre des permission restrictives sur ce fichier (chmod, chown) !!


Cas de test après déploiement
----------------------------

- Tester toutes le règles définies dans iptables et ip6tables.
- Tester la synchronisation des routes depuis coin :
    - Tous les subnet des abonnements VPN actifs doivent êtres routé vers la VM OpenVPN.
    - Le firewall doit ouvrir totalement l'accès à ces subnet.

TODO
----

Voir Fichier CONTRIBUTING
