#!/bin/bash

##################################################
#
# Main installation script
#
##################################################

set -eu
print_exit_status() {
    exit_code=$?

    trap '' EXIT

    if [[ "$exit_code" -ne 0 ]]
    then
        if [[ $1 = "main" ]]
        then
            echo "WARNING !!!!"
            echo "Erreur dans le script principal"
            echo "ARRET !!!"
        else
            echo "WARNING !!!!"
            echo "Erreur dans le script du service $1 action $2"
            echo "ARRET !!!"
        fi
        exit 1
    else
        exit 0
    fi
}
trap 'print_exit_status main' EXIT

####################
# Usage
####################

print_usage()
{
	echo "
Usage : ./main.sh action node|vm num

action :
	INSTALL
	UPDATE
	BACKUP
	RESTORE

num :
	Virtual machine or compute node number (see config folder)
"
}

####################
# Check input parameters
####################

if [[ $# -ne 3 ]] || [[ ! "$2" =~ (vm|node) ]]
then
	print_usage
	exit 0
else
	case "$1" in
	    INSTALL)
	        action=INSTALL
	    ;;
	    UPDATE)
	        action=UPDATE
	    ;;
	    BACKUP)
	        action=BACKUP
	    ;;
	    RESTORE)
	        action=RESTORE
	    ;;
	    *)
	        print_usage
		exit 0
	esac
fi

####################
# Local variables
####################

services_folder='services/'
tmp_folder='tmp/'
backup_path='backup/'
system_status_file='/etc/SNN_deploy_script_status'
system_log_file='/var/log/SNN_deploy_script.log'

sys_action=$1
sys_type=$2
sys_num=$3

echo -e "\n\n\n------------------------------"
[[ $sys_type = "vm" ]] && echo -e "Virtual machine N°$sys_num - $sys_action"
[[ $sys_type = "node" ]] && echo -e "Node N°$sys_num - $sys_action"
echo -e "------------------------------\n\n\n"

####################
# Check VM status
####################

git_branch=$(git status --branch --porcelain | grep "##" | cut -d' ' -f2 | cut -d'.' -f1)

if [[ "$action" = INSTALL ]]
then
    if [[ -e "$system_status_file" ]]
    then
        echo "Error : Il semble que vous avez déjà executé une installation sur ce système. Vous pouvez lancer l'install uniquement un fois. Il semblerais que vous devez executer l'upgrade."
        exit 1
    else
        echo "# Info for SNN deployement scripts"   >> "$system_status_file"
        echo "system_type=$sys_type"                >> "$system_status_file"
        echo "system_number=$sys_num"               >> "$system_status_file"
        echo "system_branch=$git_branch"            >> "$system_status_file"
    fi
else
    if [[ ! -e "$system_status_file" ]]
    then
        echo "Error : Il semble que vous avez pas encore executé l'install. Vous pouvez pas effectuer l'opération ${action,,} alors que quelque chose qui n'est pas encore installé."
        exit 1
    else
        sys_type_from_file=$(grep "system_type=" "$system_status_file" | cut -d'=' -f2)
        sys_num_from_file=$(grep "system_number=" "$system_status_file" | cut -d'=' -f2)
        sys_branch=$(grep "system_branch=" "$system_status_file" | cut -d'=' -f2)

        if [[ "$sys_type_from_file" != "$sys_type" ]]
        then
            echo "Error : Il semblerais que vous tentez d'executer l'action ${action,,} pas sur le bon type de système. Vous êtres sur un(e) $sys_type_from_file alors que tentez d'executer un script pour un(e) $sys_type"
            exit 1
        fi

        if [[ "$sys_num_from_file" != "$sys_num" ]]
        then
            echo "Error : Il semblerais que vous tentez d'executer l'action ${action,,} pas sur la bonne VM. La VM donné en argument est $sys_num et la VM sur laquelle vous êtes est $sys_num_from_file"
            exit 1
        fi

        if [[ "$sys_branch" != "$git_branch" ]]
        then
            echo "Error : Il semblerais que vous tentez d'executer un upgrade depuis la branche '$git_branch' alors que le serveur à été installé depuis la branche '$sys_branch'"
            echo "Voulez vous VRAIMENT CONTINUER ? [yes/no]"
            read answer
            if [[ $answer != "yes" ]]
            then
                exit 1
            fi
        fi
    fi
fi

# Save in log the actions
echo "$(date "+%d-%m-%Y %H:%M") Start ${action,,} at branch $git_branch - commit $(git log --oneline -1 | cut -d' ' -f1)" >> "$system_log_file"

####################
# Config files
####################

# All new variables will be added to ENV variables
set -a

# Import global config files
source config/sys_all/config.sh
source config/sys_all/private.sh

# Import vm specific config files
source config/${sys_type}_${sys_num}/config.sh
source config/${sys_type}_${sys_num}/private.sh

# All new variables will NOT be added to ENV variables
set +a


####################
# Load services
####################

# Load global services
services_to_call="$ROOT_SERVICES $LOCAL_SERVICES"

echo "Services to call : $services_to_call"
echo
echo


####################
# Replace all global variables in folder by name
####################
replace_var_in_folder_by_name() # $1 folder, $2 var_name
{
	folder=$1
	var_name=$2

	# Get variable text and value from name
	variableText="__"$var_name"__"
	variableValue=${!var_name}
    variableValue="${variableValue//'\'/'\\'}"
	variableValue="${variableValue//'
'/'\n'}"
    variableValue="${variableValue//'('/'\('}"
    variableValue="${variableValue//')'/'\)'}"
    variableValue="${variableValue//'['/'\['}"
    variableValue="${variableValue//']'/'\]'}"
    variableValue="${variableValue//'{'/'\{'}"
    variableValue="${variableValue//'}'/'\}'}"
    variableValue="${variableValue//'&'/'\&'}"
    variableValue="${variableValue//'.'/'\.'}"
    variableValue="${variableValue//'*'/'\*'}"

	# Find all files in the (sub)folder(s) and replace variableName by value
	find "$folder" -name \* -type f -exec sed -i "s&$variableText&$variableValue&g" {} \;
}


####################
# Replace all global variables in folder
####################
replace_all_global_var_in_folder() # $1 folder
{
	folder=$1

	# Get all global variables and loop at them
	for global_var in $(compgen -e); do

		# Replace variable name by value in this folder
		replace_var_in_folder_by_name "$folder" "$global_var"
	done

	if grep -R -P "__[A-Z\d_]+__" "$folder"/*/system_root/
	then
		echo "Error : des variables n'ont pas été défines correctement"
		exit 1
	fi
}


####################
# General folder copy method
####################
copy_folder() # $1 source, $2 destination
{
	source_folder=$1
	destination_folder=$2

   if [ -d "$source_folder" ]
   then
      if [ ! -d "$destination_folder" ]
      then
	      # Create destination folder if not exist
	      mkdir "$destination_folder"
      fi

	   # Copy files from service folder to temp folder
	   cp -r "$source_folder"/. "$destination_folder"

   else
   	echo "Warning - Copy ignored - Folder not found: $source_folder "
   fi
}


####################
# Execute actions
####################

if [[ "$action" == "INSTALL" ]] || [[ "$action" == "UPDATE" ]]
then

	# Update apt-get before install or update anything
	apt-get update
	apt-get -y dist-upgrade

	####################
	# Generate tmp folder with all services and replace global variables
	####################
	# Copy files from services folder to temp folder
	copy_folder $services_folder $tmp_folder

	# Replace global variables in tmp folder
	replace_all_global_var_in_folder $tmp_folder


	####################
	# PRE install/update
	####################
	echo -e "\n\n----------"
	echo -e "PRE$action"
	echo -e "----------"

	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"

		service_script="$tmp_folder$scriptName/$scriptName.sh"
		$service_script PRE${action} || print_exit_status $scriptName PRE${action}
	done


	####################
	# install/update
	####################
	echo -e "\n\n----------"
	echo -e "$action"
	echo -e "----------"

	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"

		#Get service folder with data to copy
		service_folder="$tmp_folder$scriptName/system_root/"

		# Copy service into root
		copy_folder $service_folder /
	done


	####################
	# POST install/update
	####################
	echo -e "\n\n----------"
	echo -e "POST$action"
	echo -e "----------"

	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"

		service_script="$tmp_folder$scriptName/$scriptName.sh"
		$service_script POST${action} || print_exit_status $scriptName POST${action}
	done

	#Remove tmp_folder
	if [ -d $tmp_folder ]
	then
		rm -rf $tmp_folder
	fi


elif [[ "$action" == "BACKUP" ]]
then
	####################
	# backup
	####################
	echo -e "\n\n----------"
	echo -e "$action"
	echo -e "----------"


	# Build backup folder path
	date_now=$(date --rfc-3339=date)
	sys_folder="$sys_type$sys_num/"
	full_backup_path="$backup_path$sys_folder$date_now"

	# Create backup directory
	mkdir -p $full_backup_path/


	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"


		# Create backup folder for each services
		service_backup_folder="$full_backup_path/$scriptName/"
		mkdir -p "$service_backup_folder"

		# Call each services and give backup path
		service_script="services/$scriptName/$scriptName.sh"
		$service_script ${action} ${service_backup_folder} || print_exit_status $scriptName ${action}
	done

elif [[ $action == "RESTORE" ]]
then

	####################
	# restore
	####################
	echo -e "\n\n----------"
	echo -e "$action"
	echo -e "----------"


	# Get backup folder
	# TODO
	#$full_backup_path=?????


	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"


		# Get backup folder for each services
		service_backup_folder="$full_backup_path/$scriptName/"

		# Call each services and give backup path
		service_script="services/$scriptName/$scriptName.sh"
		$service_script ${action} ${service_backup_folder} || print_exit_status $scriptName ${action}
	done

else

	echo "Error in main.sh"


fi

echo "$(date "+%d-%m-%Y %H:%M") Finish ${action,,} at branch $(git branch | grep '*' | cut -d' ' -f2) - commit $(git log --oneline -1 | cut -d' ' -f1)" >> $system_log_file

echo -e "\n\n----------"
echo -e "DONE $action ${sys_type}_$sys_num"
echo -e "----------"

exit 0

